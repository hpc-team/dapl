Source: dapl
Priority: optional
Section: net
Maintainer: Debian HPC Team <debian-hpc@lists.debian.org>
Uploaders: Roland Fehrenbacher <rfehren@debian.org>,
Build-Depends: debhelper (>= 10),
               dh-autoreconf,
               libibverbs-dev,
               librdmacm-dev,
Standards-Version: 4.1.3
Vcs-Git: https://salsa.debian.org/hpc-team/dapl
Vcs-Browser: https://salsa.debian.org/hpc-team/dapl
Homepage: https://www.openfabrics.org/downloads/dapl/

Package: libdapl-dev
Section: libdevel
Architecture: amd64 i386 arm64 ppc64el s390x
Depends: libdapl2 (= ${binary:Version}),
         ${misc:Depends},
Description: development files for the DAPL libraries
 The Direct Access Programming Library (DAPL) is a transport-independent,
 platform-independent API that supports Remote Direct Memory Access (RDMA)
 devices such as Infiniband and iWARP .
 .
 This package contains the header files and shared libraries for building
 applications against libdapl.

Package: libdapl2
Section: libs
Architecture: amd64 i386 arm64 ppc64el s390x
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: Direct Access Programming Library (DAPL)
 The Direct Access Programming Library (DAPL) is a transport-independent,
 platform-independent API that supports Remote Direct Memory Access (RDMA)
 devices such as Infiniband and iWARP .
  .
 This package contains the libdapl shared library.

Package: dapl2-utils
Architecture: amd64 i386 arm64 ppc64el s390x
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: utilities for use with the DAPL libraries
 The Direct Access Programming Library (DAPL) is a transport-independent,
 platform-independent API that supports Remote Direct Memory Access (RDMA)
 devices such as Infiniband and iWARP .
 .
 This package contains example utilities that use the DAPL API.
